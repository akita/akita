** This repository is deprecated in favor of https://github.com/sarchlab/akita.**

# Project Akita

Project Akita is a framework for computer architecture simulators. This repository is an architecture-agnostic simulator framework that defines essential concepts like Component and Events. 

Please see [here](doc/index.md) for detailed documentations.
